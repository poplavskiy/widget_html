function round15(moment_datetime_instance) {
  if (moment_datetime_instance.minute() < 7) {
    return moment_datetime_instance.minute(0);
  } else if (moment_datetime_instance.minute() < 22) {
    return moment_datetime_instance.minute(15);
  } else if (moment_datetime_instance.minute() < 37) {
    return moment_datetime_instance.minute(30);
  } else if (moment_datetime_instance.minute() < 52) {
    return moment_datetime_instance.minute(45);
  } else {
    moment_datetime_instance.minute(0);
    return moment_datetime_instance.hour(moment_datetime_instance.hour() + 1);
  }
}