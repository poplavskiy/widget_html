jQuery.validator.addMethod("greaterThan", function(value, element, params) {
  if ($(params[0]).val() != '') {
    if (!/Invalid|NaN/.test(moment(value, "DD.MM.YYYY HH:mm").toDate())) {
      return moment(value, "DD.MM.YYYY HH:mm").toDate() > moment(moment($(params[0]).val(), "DD.MM.YYYY HH:mm").toDate());
    }
    return isNaN(value) && isNaN($(params[0]).val()) || (Number(value) > Number($(params[0]).val()));
  };
  return true;
},'Must be greater than {1}.');


var ExternalWidget = {
  DateTimePicker: {
    StartsAtScope: function() {
      return $('#date_start');
    },
    EndsAtScope: function() {
      return $('#date_end');
    },
    GetScopeDate: function(nameParam) {
      if (nameParam === 'starts_at') {
        return ExternalWidget.DateTimePicker.StartsAtScope()
      } else if (nameParam === 'ends_at') {
        return ExternalWidget.DateTimePicker.EndsAtScope()
      }
    },
    PickerStartDate: function(nameParam) {
      if (ExternalWidget.PreBookingTime.GetParam(nameParam) !== "") {
        return moment(ExternalWidget.PreBookingTime.GetParam(nameParam)).toDate();
      } else {
        return round15(moment()).toDate();
      }
    },
    FormatParam: function(nameParam) {
      // форматирование с формата air datepicker
      return moment(ExternalWidget.DateTimePicker.GetScopeDate(nameParam).val(), "DD.MM.YYYY HH:mm").format();
    },
    ControlVisibilityEndsAt: function() {
      if (ExternalWidget.PreBookingTime.GetParam('ends_at').length > 0) {
        ExternalWidget.DateTimePicker.EndsAtScope().removeAttr('disabled');
      } else {
        ExternalWidget.DateTimePicker.EndsAtScope().attr('disabled', 'disabled');
      }
    },
    OptimalPosition: function() {
      if ($("html").scrollTop() <= ($(".select_date").offset().top - 310)) {
        return "top left";
      } else if (($(document).height() - ($(".select_date").offset().top + 310)) >= 0) {
        return "bottom left";
      } else {
        return "top left";
      }
    }
  },
  PreBookingTime: {
    SetParam: function(nameParam, newVal) {
      $("input[name='external_widget_check_dates_form[" + nameParam + "]']").val(newVal);
    },
    GetParam: function(nameParam) {
      return $("input[name='external_widget_check_dates_form[" + nameParam + "]']").val();
    },
    FormatParam: function(nameParam) {
      return moment(ExternalWidget.PreBookingTime.GetParam(nameParam)).format();
    },
    HasDiffWithPlugin: function(nameParam) {
      return ExternalWidget.DateTimePicker.FormatParam(nameParam) != ExternalWidget.PreBookingTime.FormatParam(nameParam)
    },
    ReCacheParamIfNeed: function(nameParam) {
      if (ExternalWidget.PreBookingTime.HasDiffWithPlugin(nameParam)) {
        ExternalWidget.PreBookingTime.SetParam(nameParam, ExternalWidget.DateTimePicker.FormatParam(nameParam));
      }
    }
  }
};

$(document).ready(function () {
  // images
  $('.carousel_images').slick({
    lazyLoad: 'ondemand', // нужно для более быстрой прогрузки страницы
    dots: false,
    infinite: true,
    speed: 500,
    fade: true,
    cssEase: 'linear',

    prevArrow: '<button type="button" class="slick-prev icon_prev"></button>',
    nextArrow: '<button type="button" class="slick-next icon_next"></button>'
  });

  // form
  $("#step_3").validate({
    rules: {
      "external_widget_check_dates_form[plugin_starts_at]": {
        required: true
      },
      "external_widget_check_dates_form[plugin_ends_at]": {
        required: true,
        greaterThan: ["#date_start"]
      }
    },
    messages: {
      "external_widget_check_dates_form[plugin_starts_at]": {
        required: "Укажите дату и время заезда"
      },
      "external_widget_check_dates_form[plugin_ends_at]": {
        required: "Укажите дату и время выезда",
        greaterThan: "Дата выезда меньше даты заезда"
      }
    },
    highlight: function (element) {
      $(element).closest('.form-control').removeClass('is-valid').addClass('is-invalid');
    },
    unhighlight: function (element) {
      $(element).closest('.form-control').removeClass('is-invalid').addClass('is-valid');
    },
    errorElement: "div",
    errorClass: "invalid-feedback"
  });

  $("#step_2").validate({
    rules: {
      "external_widget_pre_booking_form[client_name]": {
        required: true
      },
      "external_widget_pre_booking_form[phone_number]": {
        required: true
      },
      "external_widget_pre_booking_form[agree]": {
        required: true
      }
    },
    messages: {
      "external_widget_pre_booking_form[client_name]": "Укажите Ваше Имя",
      "external_widget_pre_booking_form[phone_number]": "Укажите Ваш номер телефона",
      "external_widget_pre_booking_form[agree]": "Подтвердите условия использования"
    },
    highlight: function (element) {
      $(element).closest('.form-control').removeClass('is-valid').addClass('is-invalid');
    },
    unhighlight: function (element) {
      $(element).closest('.form-control').removeClass('is-invalid').addClass('is-valid');
    },
    errorElement: "div",
    errorClass: "invalid-feedback"
  });

  $('label.checkbox').on('change', function () {
    $(this).toggleClass('checked');
  });

  $('#step_2 input').on('keyup blur change', function () {
    if ($('#step_2').valid()) {
      $('#step_2 button[type=submit]').prop('disabled', false);
    } else {
      $('#step_2 button[type=submit]').prop('disabled', 'disabled');
    }
  });

  // datepicker
  var date_end = ExternalWidget.DateTimePicker.EndsAtScope().datepicker({
    position: ExternalWidget.DateTimePicker.OptimalPosition(),
    startDate: ExternalWidget.DateTimePicker.PickerStartDate('ends_at'),
    date: ExternalWidget.DateTimePicker.PickerStartDate('ends_at'),
    timepicker: true,
    minutesStep: 15,
    onSelect: function (fd, d, picker) {
      // показ кнопки "Проверить доступность", при изменении дат
      // на экране ввода данных предварительного бронирования
      if ($("form#step_2").length > 0) {
        $(".js-status-free-static").hide();
        $(".js-btn-status-free-check").show();
      }

      ExternalWidget.PreBookingTime.SetParam('ends_at', moment(d).format());
    }
  }).data('datepicker');

  var date_start = ExternalWidget.DateTimePicker.StartsAtScope().datepicker({
    position: ExternalWidget.DateTimePicker.OptimalPosition(),
    startDate: ExternalWidget.DateTimePicker.PickerStartDate('starts_at'),
    date: ExternalWidget.DateTimePicker.PickerStartDate('starts_at'),
    timepicker: true,
    minutesStep: 15,
    minDate: round15(moment()).toDate(),
    onSelect: function (fd, d, picker) {
      e_d  = moment(d).add(2, 'hours').toDate();
      date_end.update({
        minDate: moment(d).add(2, 'hours').toDate(),
        date: e_d,
        startDate: e_d
      });
      date_end.selectDate(e_d);

      // показ кнопки "Проверить доступность", при изменении дат
      // на экране ввода данных предварительного бронирования
      if ($("form#step_2").length > 0) {
        $(".js-status-free-static").hide();
        $(".js-btn-status-free-check").show();
      }

      ExternalWidget.PreBookingTime.SetParam('starts_at', moment(d).format());
      ExternalWidget.PreBookingTime.SetParam('ends_at', moment(e_d).format());

    }
  }).data('datepicker');

  $("input.phone_number").inputmask('+7(999)-999-99-99', {removeMaskOnSubmit: true, autoUnmask: true});

  if ($("#step_2 .checkbox #external_widget_pre_booking_form_agree").length > 0) {
    if ($("#step_2 #external_widget_pre_booking_form_agree")[0].checked == true) {
      $("#step_2 .checkbox").addClass("checked");
    }
  }

  // показ фото в соотношении 3:2 на моб. устройствах
  if ($("body").width() <= 667) {
    var calculatedHeight = $("body").width() / 3 * 2;
    $(".slick-list").css({"max-height": calculatedHeight.toString() + "px"});
    $(".slick-slide").css({"height": calculatedHeight.toString() + "px"});
  }

  if ($(".select_date").length > 0) {
    // определение оптимальной позиции показа air datepicker
    $(window).on('scroll', function(){
      if (window.optimalDatePickerPosition !== ExternalWidget.DateTimePicker.OptimalPosition()) {
        window.optimalDatePickerPosition = ExternalWidget.DateTimePicker.OptimalPosition();
        date_start.update({position: window.optimalDatePickerPosition});
        if (ExternalWidget.PreBookingTime.GetParam("starts_at").length > 0) {
          date_start.selectDate(moment(ExternalWidget.PreBookingTime.GetParam("starts_at")).toDate());
        }
        date_end.update({position: window.optimalDatePickerPosition});
      }
    });
    window.optimalDatePickerPosition = ExternalWidget.DateTimePicker.OptimalPosition();

    // перестраховка если заглючит синхронизации в air datepicker
    $("form#step_3").on("submit", function(e) {
      ExternalWidget.PreBookingTime.ReCacheParamIfNeed("starts_at");
      ExternalWidget.PreBookingTime.ReCacheParamIfNeed("ends_at");
    });
  }
});
